#!/usr/bin/env bash
source utils/log4bash.sh
source utils/os.sh

__ubuntu_cmd__install(){
  log_info "Installing text editors...";
  log_info "Installing sublime text (stable version)...";
  wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
  sudo apt install apt-transport-https
  echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
  sudo apt update
  sudo apt install sublime-text
  log_info "Installing Atom...";
  wget -O /tmp/atom-amd64.deb https://atom.io/download/deb
  sudo dpkg -i /tmp/atom-amd64.deb
  rm /tmp/atom-amd64.deb
  log_info "Installing Visual Code...";
  wget -O /tmp/visual-code.deb https://go.microsoft.com/fwlink/?LinkID=760868
  sudo dpkg -i /tmp/visual-code.deb
  rm /tmp/visual-code.deb
  sudo apt --fix-broken install
  log_success "Instalation ${YELLOW}sublime, atom, visual-code${LOG_SUCCESS_COLOR} complete";
}


__arch_cmd__install(){
  log_info "Installing text editors...";
  log_info "Installing sublime text (stable version)...";
  curl -O https://download.sublimetext.com/sublimehq-pub.gpg && sudo pacman-key --add sublimehq-pub.gpg && sudo pacman-key --lsign-key 8A8F901A && rm sublimehq-pub.gpg
  echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf
  sudo pacman -Syu sublime-text
  log_info "Installing Atom, Code...";
  local packages="atom code"
  sudo pacman -S ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}


cmd__install(){
  __check_distro
  if [ $CURRENT_DISTRO == $ARCH_DISTRO_ID ]
  then
    __arch_cmd__install
  elif [ $CURRENT_DISTRO == $UBUNTU_DISTRO_ID ]
  then
    __ubuntu_cmd__install
  fi
}

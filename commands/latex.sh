#!/usr/bin/env bash
source utils/log4bash.sh
source utils/os.sh

__ubuntu_cmd__install(){
  log_info "Installing xetex...";
  local packages="texlive-xetex texlive-luatex texlive-lang-spanish texlive-lang-french texlive-lang-english texlive-fonts-recommended texlive-fonts-extra  "
  sudo apt install ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}

__arch_cmd__install(){
  log_info "Installing xetex...";
  local packages="texlive-bin texlive-core texlive-fontsextra texlive-langextra texlive-latexextra texlive-pstricks "
  sudo pacman -S ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}

cmd__install(){
  __check_distro
  if [ $CURRENT_DISTRO == $ARCH_DISTRO_ID ]
  then
    __arch_cmd__install
  elif [ $CURRENT_DISTRO == $UBUNTU_DISTRO_ID ]
  then
    __ubuntu_cmd__install
  fi
}